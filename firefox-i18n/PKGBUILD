pkgname=firefox-i18n
pkgver=57.0.2
pkgrel=1
pkgdesc="Language pack for Firefox"
arch=('any')
url="http://www.mozilla.com/"
license=('MPL' 'GPL')
depends=("firefox-kde=$pkgver")
makedepends=('zip' 'unzip')

_languages=(
  'ach    "Acholi"'
  'af     "Afrikaans"'
  'an     "Aragonese"'
  'ar     "Arabic"'
  'as     "Assamese"'
  'ast    "Asturian"'
  'az     "Azerbaijani"'
  'bg     "Bulgarian"'
  'bn-BD  "Bengali (Bangladesh)"'
  'bn-IN  "Bengali (India)"'
  'br     "Breton"'
  'bs     "Bosnian"'
  'ca     "Catalan"'
  'cs     "Czech"'
  'cy     "Welsh"'
  'da     "Danish"'
  'de     "German"'
  'dsb    "Lower Sorbian"'
  'el     "Greek"'
  'en-GB  "English (British)"'
  'en-US  "English (US)"'
  'en-ZA  "English (South African)"'
  'eo     "Esperanto"'
  'es-AR  "Spanish (Argentina)"'
  'es-CL  "Spanish (Chile)"'
  'es-ES  "Spanish (Spain)"'
  'es-MX  "Spanish (Mexico)"'
  'et     "Estonian"'
  'eu     "Basque"'
  'fa     "Persian"'
  'ff     "Fulah"'
  'fi     "Finnish"'
  'fr     "French"'
  'fy-NL  "Frisian"'
  'ga-IE  "Irish"'
  'gd     "Gaelic (Scotland)"'
  'gl     "Galician"'
  'gn     "Guarani"'
  'gu-IN  "Gujarati (India)"'
  'he     "Hebrew"'
  'hi-IN  "Hindi (India)"'
  'hr     "Croatian"'
  'hsb    "Upper Sorbian"'
  'hu     "Hungarian"'
  'hy-AM  "Armenian"'
  'id     "Indonesian"'
  'is     "Icelandic"'
  'it     "Italian"'
  'ja     "Japanese"'
  'ka     "Georgian"'
  'kab    "Kabyle"'
  'kk     "Kazakh"'
  'km     "Khmer"'
  'kn     "Kannada"'
  'ko     "Korean"'
  'lij    "Ligurian"'
  'lt     "Lithuanian"'
  'lv     "Latvian"'
  'mai    "Maithili"'
  'mk     "Macedonian"'
  'ml     "Malayalam"'
  'mr     "Marathi"'
  'ms     "Malay"'
  'my     "Burmese"'
  'nb-NO  "Norwegian (Bokmål)"'
  'nl     "Dutch"'
  'nn-NO  "Norwegian (Nynorsk)"'
  'or     "Oriya"'
  'pa-IN  "Punjabi (India)"'
  'pl     "Polish"'
  'pt-BR  "Portuguese (Brazilian)"'
  'pt-PT  "Portuguese (Portugal)"'
  'rm     "Romansh"'
  'ro     "Romanian"'
  'ru     "Russian"'
  'si     "Sinhala"'
  'sk     "Slovak"'
  'sl     "Slovenian"'
  'son    "Songhai"'
  'sq     "Albanian"'
  'sr     "Serbian"'
  'sv-SE  "Swedish"'
  'ta     "Tamil"'
  'te     "Telugu"'
  'th     "Thai"'
  'tr     "Turkish"'
  'uk     "Ukrainian"'
  'uz     "Uzbek"'
  'vi     "Vietnamese"'
  'xh     "Xhosa"'
  'zh-CN  "Chinese (Simplified)"'
  'zh-TW  "Chinese (Traditional)"'
)

pkgname=('firefox-i18n')
source=()
_url=https://download-installer.cdn.mozilla.net/pub/firefox/releases/${pkgver}/linux-x86_64/xpi

for _lang in "${_languages[@]}"; do
  _locale=${_lang%% *}
  _pkgname=firefox-i18n-${_locale,,}

  pkgname+=($_pkgname)
  source+=("firefox-i18n-$pkgver-$_locale.xpi::$_url/$_locale.xpi")
  eval "package_$_pkgname() {
    _package $_lang
  }"
done

# Don't extract anything
noextract=(${source[@]%%::*})

package_firefox-i18n() {
  pkgdesc="Transition package to split build"
  depends=("firefox-kde=${pkgver}" "firefox-langpack=${pkgver}")
}

_package() {
  pkgdesc="$2 language pack for Firefox"
  provides=("firefox-langpack=${pkgver}")
  conflicts=("firefox-i18n<31.0")
  install -Dm644 firefox-i18n-$pkgver-$1.xpi \
    "$pkgdir/usr/lib/firefox/browser/extensions/langpack-$1@firefox.mozilla.org.xpi"
}

sha256sums=('6e4e35ac6dd6881aceacbb897a88db4dbc35bb15e6aef9069e69e29f3d6af72a'
            '98328045407b459bc9d03cfb5fd509f65a73d373f2524727346f66b73890977f'
            'd5c1541a51b59a62ef0f76d37b23a31aea9eef5586c2a912258a1bb08b256c47'
            '501ef924d8521a64e9a477db734b50d8b30a9f40cd21da6a5f170c7623736081'
            '75d2c6d17da3ad0f3821882490c8af144cb8485db147db0af21643a14e192168'
            '8a8e1b26600833fa663d0d8ed4ab63bb33b3b4247259f1a90461005821511f34'
            '34463c8aab4571f2b45bea2a440997e1848d8175db4cf02659743947c40b7c60'
            '54dd40cfb62782b99d16ff191dcf511907844293c67b241e3b230992e0c8e783'
            '135c18d3d173dba24179935866e2334fb0eeca28993de66ac04ee7a57a241d10'
            'acfbf33249908eaed55b467077c6030fc38cda915cdf55b819548130ac153a69'
            '7c9e03f9f4080309291fe2379840d30ac19f3f9bb62e7d0ddd10fda706ef4733'
            'db1d167959c903034cd2e2645f650edd33b542b5802914a8991a617034a31ba2'
            '8c7ff8d726790aae2506dcb29af145dc0da41ab252cee38cf441680a085ce01b'
            'cc7f99940468ca3e9e11d432f87f1e04d0c6299c46fa3518a63cd61ad7bd8275'
            '0f66afc9cb1d67b235f763f15a97ea2336bf706bbca6687789c6e9fafb24da9b'
            'ae32aa8b3a172f26b13ee0909e0e4166d0a4301ed84a90c94a140f893f1fc48f'
            '3902e8f16d8dbc9071a91dc0ab4179ee2a1d4193da44e988c25a620b22b2cf8c'
            'e0520ef5f0c4d915a640b6bff9539d767703644b82818cd1efdaf60b8a9099ec'
            'bfa87f8e8675f123554792efe9c53653247c04935912c57ecda78de57f0064e0'
            'e1075d9fd62fd5131ce6b3b470c449acc9543c703210f85cbd53d780f0cc1562'
            '7c830511020b3c3d941d6b699b569a3a7a0897affe70ae4f55229af657b4696b'
            '322fc7e30545b0c417bd4065e75705fa2f388c58932bb094ea1f05e401589186'
            '6db08bf03342576572a5df73403d5329fc6f2774b1c548745a20ba0d3868b1c8'
            'eb00fbe4181b758e29192310882e90fee83ab48f4dd068e3d3c24f86862cf2e0'
            '00add571f8d89d246c64bd7dcf63a270c2b1765138cb177e2d791ad17cc6394c'
            'afcd189b83683e1f4248fd8241ac68a8b66eaaa71364864d48f0972fedc39143'
            '849511a3477a93d8d58e38181b8e5182c63119c4aa5b69253d261a7ba8b89353'
            'b6cb88eb351b3c8d52694712094ebf5441f2f9616d797268992bb7163708521b'
            'bf9b6e1998142d43e80efc56f66d485257503a360e9a68d61eff7443be64627f'
            'fe744c9d48b3b7932ba4b34ff3e7b7e945c1f5455144d074abda84763a80a17b'
            'b80774f01679539693f1a159162ce45257f373aeaa07367a59d2750772928dcb'
            'd376d8e25b4ef247fff63867bf5f4dfa8be608cc65d2d206d967e3259f2c40f4'
            'e6e26b0da88bd79b808e5c901197c032c438526334a103bcc70a22a7e96a346e'
            '5230e8782646ddfdcdb09f9efc4b060e403556a4b87ad0d0c2b304da909c57fb'
            '2f685faf86aacda1322fb663d6c39ebcdb45f9e19053e87d441bf5c89a0523fb'
            'c77188c83afcf22e8243822cdc3d51f75ce222d379de9456cc780cb9edd6aca5'
            'bd63815fac2de2d2ab769836b877e0d88a096b72e5711bfb3ffd327c6400f946'
            '4d62a918541d5b09ab0abf60a0056f9352c219d9d61315f998895c62dfed709d'
            'f65a91aee0d07dc86d517dee090d499ec0bfc80a062ac73844f2a95b5e59a0e1'
            'd551db38885246ede09facd9d1b3446f8dd1073a07997ea8daef33ce5d11ca63'
            '11468e46911f48ebde6fe663d1f426523e9dd2ee5f3a4556b9bd6f01714dbbfa'
            '4bf7695f4ed4b3bb5b76336c9c65c857db92240aef5fe05282cff42cbfbc8a87'
            '2d29f32b8e8c83444916a5d3a4abaf8d49114b9740597afed2136672894a8577'
            'c03f86bf8a1edfc1d3e5aeb7bb3f106dcdef12adb3a6960fa551d111ad5022e2'
            '08699bbd2aaa4955e9ef26486c4ed78199d47646e411aa1e8b4084f394790887'
            '9a51455e1e02532e43a15c74a33c07286fa5473c1e5f20021cdc8c303f590e8e'
            'a0b6b2214838bc1ca54fad61ec3efff6822f4549284cf279d0ac03a60d2d0bdf'
            'e6b857028976758221afb59c1a93a7f9313a0c1e481746e816e85a04f725a9fa'
            '88573b68e2f74bb58469bb294b2099ab94eded41e003d5f2528bb825fef6b68b'
            '3634f4d039fbd9bb706aa37fdaabbaf3c43a0ed9c57425ef8b91edcd41a612fa'
            '72830982f07693047cb56d2179f9ef016d7e712dbc7cafd9d2f56ae27f6d00c0'
            '89d2cfe73a574120c2e700fbf6425968be192c485158867108ff2b31dd06aff3'
            'c1d7966325c1d326311b2aa2c6df10877a8a9ef4b92977e9d4700be9646cbaff'
            '1858583b8cd7409c13de825289226330a79300c980420c8c3691c1b27daf2634'
            '8f31e6df57a7087c8347b8edfb0625727c2e6fbc4310ce31cb0ca82c1c387e24'
            'a9f55cecd5b115f653366a8f38763e57060b9bf61b15ef0cea6fd6692fbb492e'
            'e1f993470e7743543e758c7d792b2845ba30e35b9c86e2353656c20c1a7b06b6'
            'faa23874d8e1e43c644e7fb4d02bd9d54378b00b2085f6e522fe9255da65f517'
            '815b15bb7a3894e4614613079499d6fd753bc98dc3147516e296739812c4e8ae'
            '3b52ab2e01cd6db9c9f948a18deb160c86bf86e4daaf250ee813a4f46654f517'
            '0365dfaec3ac71d6daa81d6eb4a23ca71bdf146b750187b09942eae6382bcfb3'
            'e72c0d934e2585b0a99d9557630dadbb62e7c5498953a4a33c80bebbb3c3430f'
            'b5b9891ce75d0ef84f71ab8edb7f841d4cbe616d1c50ecffac138cb7a2272925'
            'eb271781dc61f109c418a0f5075fded4dd2d25dc43672b47f2d07093c02d2bc1'
            'ab79095d0df46d8ea36503a32da6bb9edca5e4d99ac0ce53ea9c83b7baefd0ab'
            '0b78ab9a6d507aa56e40ccebc09ddb5410a21191d7954903a9ae6595767e9b60'
            '28b05f2e805721362fb3c85cc7f0ca589c344642297248fa9dd9991a8e2813be'
            'ac349cbf3ae5d3f274027fa1d0a31b91566d94af2518a73079c98cc32ed0d585'
            '31bc2abc789aa25c64dbaed7bc1bf480e8ab62d705ae29afdb7b1af554e66c83'
            '8451142783244371a2e24cb28b4f61f0c710f041dfa43b0f30c3563999621a3f'
            '05f0c1bd1277c0c50fb309fccb6d88e792ad2558b103644b3fef994987f251ad'
            '7bb5eb31678a9b2e37c8d0fc5a1627d8f10cb494dfe4fa0ce784677ff27387e0'
            'a1ba6dd10d1277d19297bc63bd16e5a973152d092b1135ab18479e897735ce1a'
            '48ac606540ed9911f49f1334beff5e034deb797dd11af5520b831f6464543260'
            '50932ff3537a30bed13432dfa9bb6cf4abb7659fe08923d1c4b2b835f1d5c86c'
            '55bb41d5fdcc0fe58f4239cc31a980ceb202b4627b42e737e14170ab59e71185'
            'c64047564a88a5f4562759199a80b3ce258376f433b3d15d62cd6e7554aa63a0'
            '65db6c1662714b30ca3d5480664f02baaf80ce173c698adaeea23582927c3ef2'
            'b49c082d8410ce2b6e89db3d36ab4724ad873285b78ebc05726023eae9a2a9d0'
            '6fb3de4aab16dc7dfbfcda9f79c8be3a4c500a6e1ddaec92b4bfb770e7f3d097'
            '0c617640a865fa8a9088a502b2f9d66e7f09ff64de8feac4b1aaaae8cbbc6784'
            'dc084a3ff1c690328f87a963a38d0e807ae9f611d93fbb2f3607d15572322c84'
            '02145c407a5675645ad5cf2bfbcda2120f9a21002b0edccdb2d4acc59c91b7f1'
            'ee88ed22294610d530da21df6ba033c15cf105af7f1197de0cf90baa05e6d06a'
            '1eece283a06a36c6dd206b022672ef7c25f77bc9827f5a7e1dbcdf823948786c'
            '35a1e1235b6b8c2f2c1d72e7c189b78bbe2e979dc935bfd7e0100805e934e381'
            'b0dfbc6cd5737715a9c80f344e37ec3a07244b2f2c3bb9e6a34b4dbdc23b9247'
            'f5dd58659d4168785f9bb4eb3c32dfc3c84d75f943cd8a7b313e9a18b39eebfb'
            'f14dff5567ce4c9b248ae3078291e0a1d9b17961d0e5b8332bdf0bfa2a811a67'
            '3368bb712e1234970daef8b8fa261112218109a652e5892768ad2012bfe8765d'
            '3bec2ff2bb4ced1b578c00508fe49874527d4e4f7ad8bc1fec26e11410751f76'
            'fced0d1ad64213ee29d98ef40fa3226f462e6a3509f7e2ca9bad2b87f46c68fb')
